
component name="Test"
{
    public function getMyVarElvis(cookieVar)
    {   
        // not thread safe!
        return arguments.cookieVar ?: "boo";
    }

    public function getMyVarTernary(cookieVar)
    {   
        // thread safe
        return !isNull(arguments.cookieVar) ? arguments.cookieVar : "boo";
    }
}