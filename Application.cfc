component {
    function onApplicationStart()
    {
        application.testService = new services.Test();
    }
    function onRequestStart(targetPage)
    {
        if (!isDefined("cookie.myVar"))
        {
            cookie.myVar = createUUID();
        }
    }
}