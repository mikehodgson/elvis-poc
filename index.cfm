<cfscript>
    // Run Apache Bench against this file with the following command line:
    // ab -n 100000 -c 200 http://localhost/elvis-poc/index.cfm

    // After execution:
    // _elvis_mismatch.log should contain results
    // _ternary_mismatch.log should not exist, or contain 0 results

    local.elvVar = application.testService.getMyVarElvis(cookie.myVar);
    local.ternVar = application.testService.getMyVarTernary(cookie.myVar);

    local.isElvisMatch = (cookie.myVar == local.elvVar ? true : false);
    local.isTernaryMatch = (cookie.myVar == local.ternVar ? true : false);

    if (!local.isElvisMatch)
    {
        writeLog(file = '_elvis_mismatch', text = serializeJSON({myVar: cookie.myVar, elvVar: local.elvVar, isElvisMatch: local.isElvisMatch, dateTime: now()}));
    }
    if (!local.isTernaryMatch)
    {
        writeLog(file = '_ternary_mismatch', text = serializeJSON({myVar: cookie.myVar, ternVar: local.ternVar, isTernaryMatch: local.isTernaryMatch, dateTime: now()}));
    }
</cfscript>
<cfoutput>
<html>
    <head>
        
    </head>
    <body>
        cookie value: #cookie.myVar#<br>
        function return value using Elvis: #local.elvVar#<br>
        function return value using Ternary: #local.ternVar#<br>
    </body>
</html>
</cfoutput>